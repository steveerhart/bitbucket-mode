(mapcar 'require
        '(cl
          json
          url))


(defvar api-prefix "https://api.bitbucket.org/1.0/")

(defun form-encode (form)
  "(bb-form-encode '((arg1 . arg2))) => arg1=arg2"
  (filter-params (mapconcat (lambda (x)
                              (format "%s=%s" (url-hexify-string (symbol-name (car x))) (url-hexify-string (cdr x))))
                            form
                            "&")))


(defun filter-lst (condp lst)
  (delq nil (mapcar (lambda (el)
                      (and (funcall condp el) el))
                    lst)))

(defun filter-params (lst)
  (filter-lst (lambda (alst)
                (if (cdr alst)
                    alst
                  nil))
              lst))

(defun get-method (sym)
  (cdr (assoc sym '((all . "GET")
                    (get . "GET")
                    (update . "PUT")
                    (delete . "DELETE")
                    (create . "POST")))))

(defun params-encode (form)
  "Prepends question mark onto string for passing parameters"
  (concat "?" (form-encode form)))

(defun make-request (method url params)
  (let* ((url-request-method method))
    (if (string= "GET" method)
        (url-retrieve-synchronously (concat "?" url params))
      (let* ((url-request-extra-headers '(("Content-Type" . "application/x-www-form-urlencoded")))
             (url-request-data params))
        (url-retrieve-synchronously url)))))


(defun symbol-join (syms)
  (intern (mapconcat 'symbol-name syms "-")))

(defun create-url (pattern args)
  (if args
      (create-url (replace-in-string pattern (symbol-name (caar args)) (cdar args))
                  (cdr args))
    (concat api-prefix pattern)))


(setq url-debug t)

(defmacro bb-rest-defun (name func-lst pattern)
  `(progn ,@(mapcar (lambda (func)
                      (destructuring-bind
                          (method pargs req opt)
                          func
                        `(defun ,(symbol-join (list method name)) (,@(append pargs req) &optional &key ,@opt)
                           (make-request
                            (get-method (quote ,method))
                            (create-url ,pattern (mapcar* 'cons (quote ,pargs) (list ,@pargs)))
                            (form-encode  (mapcar* 'cons (quote ,(append req opt)) (list ,@(append req opt))))))))
                    func-lst)))



(defun parse-response (buffer)
  "Takes a http response buffer and converts the JSON recieved into a hash-table"
  (let ((json-object-type 'hash-table))
    (unwind-protect
        (with-current-buffer buffer
          (progn
            (goto-char (1+ url-http-end-of-headers))
            (json-read-from-string (buffer-substring (point) (point-max)))))
      (kill-buffer buffer))))




(bb-rest-defun repositories-issues
               ((all (account repo-slug) () (limit start search sort title content version milestone component kind status responsible reported_by))
                (create (account repo-slug) (title content) (status priority responsible kind component milestone version)))
               "repositories/account/repo-slug/issues")

(setq somehash (parse-response (url-retrieve-synchronously "https://api.bitbucket.org/1.0/repositories/steveerhart/test-repo/issues")))

(defun get-avatar (url)
  (unwind-protect
      (with-current-buffer (url-retrieve-synchronously url)
        (progn
          (goto-char (point-min))
          (re-search-forward "^$" nil 'move)
          (forward-char)
          (create-image (buffer-substring (point) (point-max))
                        'jpeg
                        t)))))
