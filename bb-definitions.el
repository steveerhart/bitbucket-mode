;;;; repositories Endpoint

;;; issues Resource

(bb-rest-defun repositories-issues
               ((all (account repo-slug) () (limit start search sort title content version milestone component kind status responsible reported_by))
                (create (account repo-slug) (title content) (status priority responsible kind component milestone version)))
               "repositories/account/repo-slug/issues")

(bb-rest-defun repositories-issues
               ((get (account repo-slug) (issue-id) ())
                (update (account repo-slug issue-id) () (status priority title responsible content kind component milestone version))
                (delete (account repo-slug issue-id) () ()))
               "repositories/account/repo-slug/issues/issue-id")

;; issues comments

(bb-rest-defun repositories-issues-comments
               ((all (account repo-slug issue-id) () ())
                (create (account repo-slug issue-id) (content) ()))
               "repositories/account/repo-slug/issues/issue-id/comments")

(bb-rest-defun repositories-issues-comments
               ((get (account repo-slug issue-id comment-id) () ())
                (update (account repo-slug issue-id comment-id) (content) ())
                "repositories/account/repo-slug/issues/issue-id/comments/comment-id"))