(require 'ewoc)

(defvar bb-issues-mode-hook  nil
  "Hooks to run upon entering bb-issues-mode")
(defvar bb-issues-mode-map nil
  "Keymap for bb-issues-mode")
(defvar bb--issues-view nil
  "EWOC for bb-issues-mode")

(defconst bb--issues-header-format "%-20s  %-12s %-10s %-20s %-10s %-20s")


(defsubst bb--issues-header ()
  ;; Put spaces above the scrollbar and the fringe
  (format
   (concat (make-string (+ (scroll-bar-columns 'left) (fringe-columns 'left))
                        ? )
           git--status-header-format)
   "Title" "Type" "Priority" "Assignee" "Created" "Last Updated"))

(defsubst bb--issues-buffer-name (repo)
  (format "*bb-issues for %s*" repo))

(defsubst bb--create-issues-buffer (repo)
  (let* ((issues-buffer-name (bb--issues-buffer-name repo))
         (issues-buffer (get-buffer issues-buffer-name)))
    (or issues-buffer (get-buffer-create issues-buffer-name))))

(defsubst bb--kill-issues-buffer (repo)
  (kill-buffer (bb--issues-buffer-name repo)))

(defun bb--render-issues (info)
  (insert (format bb--issues-header-format
                  "a" "b" "c" "d" "e" "f")))
                  

;; issues mode

(defun bb-issues-mode ()
  "Major mode for viewing and editing the state of a bb directory."
  
  (kill-all-local-variables)
  (buffer-disable-undo)

  ;; set major mode
  (setq mode-name "Bitbucket-Issues")
  (setq major-mode 'bb-issues-mode)

  (use-local-map bb-issues-mode-map)

  (setq buffer-read-only t)
  (setq header-line-format (bb--issues-header))

  ;; create ewoc for current bb-issues buffer
  (set (make-local-variable 'bb--issues-view)
       (ewoc-create 'bb--render-issues "" "")))



;  (run-hooks 'bb-issues-mode-hook))

;; Autoloaded entry point
(defun bb-issues (repo)
  "Launch bb-issues-mode on the specified repo. With a prefix
Argument (C-u), always prompts."
  (interactive "SRepository Name: ")
  (progn
    (switch-to-buffer (bb--create-issues-buffer repo))
    (bb-issues-mode)))
  
